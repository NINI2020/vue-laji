
import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation_types'//导入
 
 
Vue.use(Vuex);
 
const store = new Vuex.Store({
  state: {
    footerVisible: true,//底部bar显示的状态
    selectedTab: 'main'//选中的tab标签
  },
  mutations: {
    [types.TOGGLE_FOOTER] (state) {
      state.footerVisible = !state.footerVisible
    },
    [types.SELECT_TAB](state, val){
      state.selectedTab = val
    }
  }
});
export default store