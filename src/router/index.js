import Vue from 'vue'
import Router from 'vue-router'
import Start from '../pages/start/Start'
import Home from '../pages/home/Home'
import My from '../pages/my/My'
import Shop from '../pages/shop/Shop'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'start',
      component: Start
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
     {
      path: '/my',
      name: 'my',
      component: My
     },
     {
      path: '/shop',
      name: 'shop',
      component: Shop
     },
    
  ],
  mode:'history'  //去掉路径的#
})
